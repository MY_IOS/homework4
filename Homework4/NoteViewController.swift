//
//  NoteViewController.swift
//  Homework4
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController {

    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var note: UITextView!
    @IBOutlet weak var txtNote: UITextView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    lazy var context = appDelegate.persistentContainer.viewContext
    lazy var noteObject = Note(context: context)
    
    //
    var updateNote: Note!
    var isUpdated = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNote.text = "Note"
        txtNote.textColor = .lightGray
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isUpdated == false{
             saveNoteData()
        }
        else{
             updateNotes()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if isUpdated == true{
            titleText.text = updateNote.desc!
            txtNote.text = updateNote.title!
        }
    }
    
    func saveNoteData(){
        if txtNote.text! != "" || titleText.text! != ""{
            let noteObject = Note(context: context)
            noteObject.setValue(titleText.text!, forKey: "title")
            noteObject.setValue(txtNote.text!, forKey: "desc")
            appDelegate.saveContext()
        }
        
    }
    //
    func updateNotes() {
        if isUpdated == true{
            updateNote.setValue(titleText.text!, forKey: "title")
            updateNote.setValue(txtNote.text!, forKey: "desc")
            appDelegate.saveContext()
        }
        
    }

}
extension NoteViewController: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if  txtNote.text == "Note"{
            txtNote.textColor = .black
            txtNote.text = ""
            
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if  txtNote.text == ""{
            txtNote.textColor = .lightGray
            txtNote.text = "Note"
            
        }
    }
}
