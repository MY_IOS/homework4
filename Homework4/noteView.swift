//
//  ViewController.swift
//  Homework4
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    var arrNote: [Note] = []
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    lazy var context = appDelegate.persistentContainer.viewContext
    
    
    var deleteCellindex = -1
    @IBOutlet weak var collectionViewContainer: UICollectionView!
    @IBOutlet var tap: UITapGestureRecognizer!
    
    @IBAction func createNewForm(_ sender: Any) {
        guard  let noteView =
            self.storyboard?.instantiateViewController(withIdentifier: "noteView") as? NoteViewController
            else {
                    fatalError("View Controller not found")
            }
        navigationController?.pushViewController(noteView, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longPressToDelete = UILongPressGestureRecognizer(target: self, action: #selector(handleDelete))
        longPressToDelete.delegate = self
        collectionViewContainer.addGestureRecognizer(longPressToDelete)
        
    }
    
    
    func addNotesToArray()  {
        let allNoteObjects = NSFetchRequest<NSFetchRequestResult>(entityName: "Note")
        allNoteObjects.returnsObjectsAsFaults = true
        do{
            let noteNumber = try context.fetch(allNoteObjects)
            for note in noteNumber as! [Note]{
                arrNote.append(note)
            }
        }catch{
            print("No Note")
        }
    }
    
    
    
    
}


extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNote.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath)
        -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reusableCell", for: indexPath) as! NoteCellCollectionViewCell
        cell.title.text = arrNote[indexPath.row].title
        cell.desc.text = arrNote[indexPath.row].desc
        return cell
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrNote = []
        collectionViewContainer.reloadData()
        addNotesToArray()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let note = arrNote[indexPath.row]
        // send to updateViewController
        let updateform = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "noteView") as! NoteViewController
        updateform.updateNote = note
        print(note.desc!)
        updateform.isUpdated = true
        navigationController?.pushViewController(updateform, animated: true)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//
////        print(arrNote[indexPath.row].desc!)
////        print(arrNote[indexPath.row].title!)
////
////        guard  let noteView =
////            self.storyboard?.instantiateViewController(withIdentifier: "noteView") as? NoteViewController
////            else {
////                fatalError("View Controller not found")
////        }
////        noteView.titleText.text = arrNote[indexPath.row].desc
////        noteView.txtNote.text = arrNote[indexPath.row].title
////        navigationController?.pushViewController(noteView, animated: true)
//    }


}


extension ViewController: UIGestureRecognizerDelegate{
    @objc func handleDelete(_ sender: UILongPressGestureRecognizer) {
        if(sender.state != UILongPressGestureRecognizer.State.ended){
            return
        }
        let index = sender.location(in: self.collectionViewContainer)
        if let indexPath : IndexPath = (self.collectionViewContainer.indexPathForItem(at: index)) {
             deleteOption(index: indexPath)
        }
     
    }
    @objc func deleteOption(index :IndexPath){
        
        let alert = UIAlertController(title: "Information", message: "Are you sure to delete ?", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
                print("Delete")
                self.context.delete(self.arrNote[index.row])
                self.arrNote.remove(at: index.row)
                self.collectionViewContainer.deleteItems(at: [index])
                self.collectionViewContainer.reloadData()
                self.appDelegate.saveContext()
                
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


